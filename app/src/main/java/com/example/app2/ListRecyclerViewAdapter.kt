package com.example.app2


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
//generics
class ListRecyclerViewAdapter: RecyclerView.Adapter<ListViewHolder>() {

    val mainList = arrayOf("Shopping List","Homework","Chores")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder,parent,false)

        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mainList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listId.text = (position + 1).toString()
        holder.listTitle.text = mainList[position]
    }


}
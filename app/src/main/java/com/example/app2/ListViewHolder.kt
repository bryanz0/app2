package com.example.app2

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
//herencia
class ListViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){

    val listId = itemView.findViewById<TextView>(R.id.item_id)
    val listTitle = itemView.findViewById<TextView>(R.id.item_title)

}